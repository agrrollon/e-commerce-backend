const express = require("express")
const route = express.Router() 
const auth = require("../auth")
const ItemController = require("../controllers/ItemController")

//user routes
route.get("/", (req, res) => {
	ItemController.getAllActiveItems().then(resultFromGetAllActive => res.send(resultFromGetAllActive))
})

route.get("/:itemId", (req, res) => {
	ItemController.getItem().then(result => res.send(result))
})

//admin routes
route.get("/admin", auth.verify, (req, res) => {
	ItemController.getAllItems().then(result => res.send(result))
})

route.post("/", auth.verify, (req, res) => {
	ItemController.addItem(req.body).then(result => res.send(result))
})

route.put("/:itemId", auth.verify, (req, res) => {
	ItemController.updateItem(itemId, req.body).then(result => res.send(result))
})

route.delete("/:itemId", auth.verify, (req, res) => {
	ItemController.archiveItem(itemId).then(result => res.send(result))
})

module.exports = route