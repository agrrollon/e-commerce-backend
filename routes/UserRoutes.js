const express = require("express")
const route = express.Router()
const auth = require("../auth")
const UserController = require("../controllers/UserController")

route.post("/email-exists", (req, res) => {
	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})

//Registration
route.post("/", (req, res) => {
	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

//Login
//post is used since we send the data to create a logged in session
route.post("/login", (req, res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

module.exports = route