const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
	//userid, items array
	userId : {
		type : String,
		required : [true, "User ID is required"]
	},
	total : {
		type : Number,
		required : [true, "Total is required"]
	},
	items : [
		{
			itemId : {
				type: String,
				required : [true, "Item ID is required"]
			},
			quantity : {
				type : Number,
				required : [true, "Quantity is required"]
			},
			subtotal : {
				type : Number,
				required: [true, "Subtotal is required"]
			}
		}
	]
})

module.exports = mongoose.model("Order", orderSchema)
