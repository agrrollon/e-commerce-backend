const mongoose = require("mongoose")

const itemSchema = new mongoose.Schema({
	//name, price, description, isActive
	name : {
		type : String,
		required : [true, "Name is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"] 
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	}
})

module.exports = mongoose.model("Item", itemSchema)