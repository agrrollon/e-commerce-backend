const Item = require("../models/Item")

//functions for item controller
// View all active items
module.exports.getAllActiveItems = () => {
	return Item.find({isActive : true}).then(resultFromFind => resultFromFind)
}

// View all items
module.exports.getAllItems = () => {
	return Item.find({}).then(resultFromFind => resultFromFind)
}

//Create a new item
module.exports.addItem = (params) => {
	let newItem = new Item({
		name : params.name,
		price : params.price,
		description : params.description
	})

	return newItem.save().then((item, err) => {
		return (err) ? false : true
	}) 
}

// Get an item
module.exports.getItem = (params) => {
	return Item.findById(params.itemId).then(resultFromFindById => resultFromFindById)
}

//Update an item
module.exports.updateItem = (itemId, params) => {
	let updatedItem = {
		name : params.name,
		price : params.price,
		description : params.description
	}

	return Item.findByIdAndUpdate(itemId, updatedItem).then((item, err) => {
		return (err) ? false : true
	})
}


//deactivate an item
module.exports.archiveItem = (itemId) => {
	let updateActive = {
		isActive : false
	}

	return Item.findByIdAndUpdate(itemId, updateActive).then((item, err) => {
		return (err) ? false : true
	})
}