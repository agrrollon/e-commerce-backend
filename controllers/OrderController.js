const Item = require("../models/Item")
const Order = require("../models/Order")

//functions
//Create order
module.exports.createOrder = (params) => {
	let newOrder = new Order({
		userId : params.userId,
		total : params.total,
		items : params.items
	})

	return newOrder.save().then((order, err) => {
		return (err) ? order : "Error in saving"
	}) 
}