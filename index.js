//dependencies setup
require('dotenv').config()
const express = require("express")
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require("./routes/UserRoutes")
const itemRoutes = require("./routes/ItemRoutes")
const orderRoutes = require("./routes/OrderRoutes")

//database connection
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://admin:tsq2rwbj@cluster0.vbiww.mongodb.net/miniCapstone?retryWrites=true&w=majority', { 
	useNewUrlParser: true, 
	useUnifiedTopology: true 
})

//server setup
const app = express()
const port = 3000
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors()) //This will be used if other sites would use this API

app.use("/api/users", userRoutes)
app.use("/api/items", itemRoutes)
app.use("/api/orders", orderRoutes)

//server listening
app.listen(process.env.PORT || port, () => console.log(`Listening to port ${port}`))